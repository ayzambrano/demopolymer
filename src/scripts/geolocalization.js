
    var nav = null;

    function requestPosition() {
        if (nav == null) {
            nav = window.navigator;
        }

        var geoloc = nav.geolocation;
        if (geoloc != null) {
            geoloc.getCurrentPosition(successCallback, errorCallback);
        }

    }

    function successCallback(position) {
     document.getElementById("fecha").innerHTML =  new Date(  position.timestamp);

    }

    function errorCallback(error) {
        var strMessage = "";

        // Check for known errors
        switch (error.code) {
            case error.PERMISSION_DENIED:
                strMessage = "Access to your location is turned off. "  +
                    "Change your settings to turn it back on.";
                break;
            case error.POSITION_UNAVAILABLE:
                strMessage = "Data from location services is " +
                    "currently unavailable.";
                break;
            case error.TIMEOUT:
                strMessage = "Location could not be determined " +
                    "within a specified timeout period.";
                break;
            default:
                break;
        }

        document.getElementById("fecha").innerHTML = strMessage;
    }

    requestPosition();
